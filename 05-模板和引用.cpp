#include <iostream>

using namespace std;

template<typename T,int N>
void sort(T (&arr)[N])
{
	for(int i =0;i<N;i++)
	{
		for(int j = i;j<N;j++)
		{
			if(arr[i]>arr[j])
			{
				T temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
}

int main()
{
	int array[] = {3,6,4,5,8,2};
	
	cout<<"排序前的数组：";
	int length = sizeof(array)/sizeof(array[0]);
	for(int i = 0;i<length;i++)
		cout << array[i]<<" ";
	cout << endl;
	
	sort(array);
	cout<<"排序后的数组：";
	for(int i = 0;i<length;i++)
		cout << array[i]<<" ";
	cout << endl;
	return 0;
}