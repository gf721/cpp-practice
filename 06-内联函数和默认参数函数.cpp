#include <iostream>


template<typename T>
inline T max(T t1,T t2)
{
	if(t1>t2)
	{
		return t1;	
	}
	else
	{
		return t2;
	}
}

inline int max(int t1,int t2,int t3=3)
{
	if(t1>t2)
	{
		return t1;	
	}
	else
	{
		return t2;
	}
}

int main(int argc, char *argv[])
{
	double x=5.5,y=6.0;
	std::cout << "The max number between "<<x<<" and "<<y<<" is "<<max(x,y)<<".";
	
	int j = -10, k = -20, l= -30;
	std::cout << "The max number between "<<j<<" and "<<k<<" is "<<max(x,y)<<".";
	return 0;
}