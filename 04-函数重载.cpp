#include <iostream>

using namespace std;

const double PI = 3.1415926535;
double area(double radius)
{
	return radius*radius*PI;
}

double area(double width,double height)
{
	return height * width;
}

int main()
{
	double r,h,w;
	cout<<"请输入圆的半径：";
	cin>>r;
	cout<<"圆的面积为："<<area(r)<<endl;
	cout<<"请输入矩形的长和宽：";
	cin>>w>>h;
	cout<<"矩形的面积为："<<area(h,w);
}