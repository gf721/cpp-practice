#include <iostream>

using namespace std;

int main()
{
	const double PI = 3.1415;
	int r;
	//输入圆的半径
	cout << "请输入圆的半径r：";
	cin >> r;
	cout << endl << "圆的周长为：" << 2*r*PI << "，面积为：" << r*r*PI;
}